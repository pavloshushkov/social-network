from datetime import datetime

from dateutil import rrule
from django.conf import settings
from django.db.models import Count, DateField
from django.db.models.functions import TruncDay
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.post.exceptions import AlreadyLikedException
from apps.post.filters import LikeFilterSet
from apps.post.models import Like, Post
from apps.post.serializers import LikeSerializer, PostSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action in {"list", "retrieve", "like", "unlike"}:
            return queryset

        # User can only edit/delete his own posts
        return queryset.filter(author_id=self.request.user.id)

    @action(methods=("POST",), detail=True, url_path="like")
    def like(self, request, pk):
        post = self.get_object()
        _, created = Like.objects.get_or_create(user=request.user, post=post)
        if not created:
            raise AlreadyLikedException()
        return Response(status=status.HTTP_201_CREATED)

    @action(methods=("POST",), detail=True, url_path="unlike")
    def unlike(self, request, pk):
        post = self.get_object()
        post.likes.filter(user=request.user).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LikeViewSet(viewsets.GenericViewSet):
    queryset = Like.objects.all()
    permission_classes = (IsAuthenticated,)
    http_method_names = ("get",)
    filter_backends = (DjangoFilterBackend,)
    filterset_class = LikeFilterSet

    @action(
        methods=("GET",),
        detail=False,
        url_path="analytics",
        serializer_class=LikeSerializer,
    )
    def analytics(self, request):
        """Return likes amount per day in specified range."""
        date_format = settings.REST_FRAMEWORK["DATE_FORMAT"]
        queryset = (
            self.filter_queryset(self.get_queryset())
            .annotate(date=TruncDay("created_at", output_field=DateField()))
            .values("date")
            .annotate(amount=Count("id"))
            .values_list("date", "amount")
        )

        data = {}
        # Set default value for dates with no likes
        if {"date_from", "date_to"}.issubset(set(request.query_params.keys())):
            for day in rrule.rrule(
                freq=rrule.DAILY,
                interval=1,
                dtstart=datetime.strptime(request.query_params["date_from"], date_format),
                until=datetime.strptime(request.query_params["date_to"], date_format),
            ):
                data[day.strftime(date_format)] = 0

        for date, amount in queryset:
            data[date.strftime(date_format)] = amount

        return Response(data=data)
