# Social Network
Sample project of social network
### Basic functionality:
- Create post
- Mark post as liked
- Show user api requests activity
- Show analytics about how many likes was made

## Setup the project locally
```bash
# Create env
python3 -m venv venv

# Activate environment
. venv/bin/activate

# Install requirements
pip install -r requirements.txt

# Create DB
touch db.sqlite3

# Apply migrations
./manage.py migrate

# Run project
./manage.py runserver
```

## Bot usage for generating data
```bash
# Create local config.yaml
cp bot/config.yml.tmpl bot/config.yml

# Run script
python3 bot/entry.py
```