from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from apps.authentication.models import User


def get_tokens(user):
    refresh = RefreshToken.for_user(user)
    return str(refresh.access_token), str(refresh)


class AuthenticationViewTestCase(APITestCase):
    def test_signup(self):
        url = reverse("signup")
        data = {"username": "John", "password": "Qwerty@123"}

        r = self.client.post(url, data)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["username"], "John")

    def test_login(self):
        url = reverse("login")
        data = {"username": "John", "password": "Qwerty@123"}
        User.objects.create_user(**data)

        r = self.client.post(url, data)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertIn("access", r.data)
        self.assertIn("refresh", r.data)

    def test_refresh_token(self):
        url = reverse("refresh-token")

        data = {"username": "John", "password": "Qwerty@123"}
        user = User.objects.create_user(**data)
        refresh = RefreshToken.for_user(user)
        access, refresh = str(refresh.access_token), str(refresh)

        r = self.client.post(url, {"refresh": refresh}, HTTP_AUTHORIZATION=f"Bearer {access}")
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertIn("access", r.data)
        self.assertIn("refresh", r.data)

        # Old refresh token was blocked
        r = self.client.post(url, {"refresh": refresh}, HTTP_AUTHORIZATION=f"Bearer {access}")
        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_activity(self):
        admin = User.objects.create_superuser(username="admin", password="Qwerty@123")
        access, _ = get_tokens(admin)

        data = {"username": "John", "password": "Qwerty@123"}
        user = User.objects.create_user(**data)

        # User hasn't sent requests yet
        r = self.client.get(
            reverse("user-activity", args=(user.id,)), HTTP_AUTHORIZATION=f"Bearer {access}"
        )
        self.assertIsNone(r.data["last_login"])
        self.assertIsNone(r.data["last_request"])

        # User has already logged in
        user_access, user_refresh = get_tokens(user)
        self.client.post(reverse("login"), data, HTTP_AUTHORIZATION=f"Bearer {user_access}")
        r = self.client.get(
            reverse("user-activity", args=(user.id,)), HTTP_AUTHORIZATION=f"Bearer {access}"
        )
        self.assertTrue(r.data["last_login"])
        self.assertIsNone(r.data["last_request"])

        # User has already sent request
        r = self.client.get(
            reverse("post-list"),
            HTTP_AUTHORIZATION=f"Bearer {user_access}",
        )
        r = self.client.get(
            reverse("user-activity", args=(user.id,)), HTTP_AUTHORIZATION=f"Bearer {access}"
        )
        self.assertTrue(r.data["last_login"])
        self.assertTrue(r.data["last_request"])
