from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.authentication.models import User
from apps.authentication.serializers import SignUpSerializer, UserActivitySerializer


class SignupView(APIView):
    permission_classes = (AllowAny,)
    http_method_names = ("post",)

    def post(self, request):
        serializer = SignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.validated_data)


class UserActivityView(viewsets.GenericViewSet):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated, IsAdminUser)
    http_method_names = ("get",)

    @action(
        methods=("GET",), detail=True, url_path="activity", serializer_class=UserActivitySerializer
    )
    def activity(self, request, pk):
        """Return user last login and last request information."""
        user = self.get_object()
        return Response(self.get_serializer(instance=user).data)
