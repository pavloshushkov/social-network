from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from apps.authentication.models import User


class SignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "password")
        extra_kwargs = {"password": {"write_only": True}}

    def validate_password(self, password):
        validate_password(password=password)
        return password

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class UserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("last_login", "last_request")
